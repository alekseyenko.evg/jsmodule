const cssWidth = 200;
let events = [];
let ids = events.length;

const addEvent = (task, time, duration) => {
  events.push({
    id: ids++,
    task,
    time,
    duration
  });

  renderCalendar();
};

const renderCalendar = () => {
  const evEls = document.querySelectorAll(".calendar__event");
  for (const ev of evEls) {
    ev.remove();
  }

  renderEvent(events);
};

function renderEvent(events) {
  events.sort((a, b) => a.time - b.time);
  events.sort((a, b) => {
    if (a.time === b.time) {
      return b.duration - a.duration;
    }
    return false;
  })

  const tempEvents = JSON.parse(JSON.stringify(events));

  let cols = getCols(tempEvents);
  console.log(cols);

  const width = cssWidth / cols.length;

  for (i = 0; i < cols.length; i++) {
    for (j = 0; j < cols[i].length; j++) {
      let left = 50 + width * i;
      let height = 0;
      const eventEl = document.createElement("div");
      eventEl.dataset.id = cols[i][j].id;
      eventEl.classList.add("calendar__event");
      eventEl.style.top = `${cols[i][j].time * 2}px`;
      // eventEl.style.height = `${cols[i][j].duration * 2}px`;
      for (num = 0; num < events.length; num ++) {
        if (events[num].id === cols[i][j].id) {
          height = events[num].duration;
          break;
        }
      }
      eventEl.style.height = `${height * 2}px`;
      eventEl.innerText = cols[i][j].task;
      eventEl.style.width = `${width}px`;
      eventEl.style.left = `${left}px`;

      const calendarEl = document.querySelector(".calendar");
      calendarEl.appendChild(eventEl);
      eventEl.addEventListener("click", (ev) => {
        removeEvent(ev.target.dataset.id);
      });
    }
  }
}

function removeEvent(id) {
  events = events.filter((el) => {
    return id != el.id;
  });

  renderCalendar();
}

function getCols(tempEvents) {
  let columns = [];
  for (i = 0; i < tempEvents.length; i++) {
    let currentEvent = tempEvents[i];
    currentEvent.duration += currentEvent.time;
    if (columns.length === 0) {
      columns.push([currentEvent]);
      continue;
    }
    let end = false;
    let j = 0;
    while (!end && j < columns.length) {
      let k = 0;
      let rowEnd = false;
      while (!end && !rowEnd && k < columns[j].length) {
        if (currentEvent.time >= columns[j][k].duration && columns[j].length - 1 === k) {
          columns[j].push(currentEvent);
          end = true;
          continue;
        } else if (currentEvent.time >= columns[j][k].duration && columns[j].length - 1 !== k) {
          if (columns[j][k + 1].time > currentEvent.time && columns[j][k + 1].time >= currentEvent.duration) {
            columns[j].splice(k, 0, currentEvent);
            end = true;
            continue;
          }
        }
        if (currentEvent.time < columns[j][k].duration) {
          if (columns[j + 1] === undefined) {
            columns.push([currentEvent]);
            end = true;
            continue;
          } else {
            rowEnd = true;
            continue;
          }
        }
        k++;
      }
      j++;
    }
  }
  return columns;
}

addEvent("Exercise", 0, 60);
addEvent("Code review", 100, 15);
addEvent("Plan day", 30, 20);
addEvent("Open github", 40, 40);

const inputName = document.getElementById("input-name");
const inputTime = document.getElementById("input-time");
const inputDuration = document.getElementById("input-duration");
const addBtn = document.getElementById("btn-add-event");


addBtn.addEventListener("click", function () {
  const name = inputName.value;
  const time = inputTime.value;
  const duration = inputDuration.value;

  addEvent(name, time, duration);
});